import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLJPanel;
import javax.swing.*;

public class MainForm extends JFrame {
    private JSpinner xStartSpinner;
    private JSpinner yStartSpinner;
    private JSpinner xEndSpinner;
    private JSpinner yEndSpinner;
    private JButton mainButton;
    private JPanel rootPanel;
    private GLJPanel gljPanel;
    private JSpinner sizeSpinner;

    public MainForm(){
        super("Алгоритм Брезенхэма");
        setContentPane(rootPanel);
        pack();
        xStartSpinner.setValue(-4);
        yStartSpinner.setValue(-4);
        sizeSpinner.setValue(20);
        gljPanel.addGLEventListener(new GLEventListener() {
            @Override
            public void reshape(GLAutoDrawable canvas, int x, int y, int width, int height) {
                Renderer.setup(canvas.getGL().getGL2(), width, height, readSize());
            }
            @Override
            public void init(GLAutoDrawable canvas) {
            }
            @Override
            public void dispose(GLAutoDrawable canvas) {
            }
            @Override
            public void display(GLAutoDrawable canvas) {
                Renderer.render(canvas.getGL().getGL2(), readCoords(), readSize());
            }
        });

        mainButton.addActionListener(actionEvent -> gljPanel.display());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    protected int readSize(){
        int size = (int) sizeSpinner.getValue();
        return 2*size+1;
    }

    /**
     * упаковка координат из спиннеров
     * @return массив координат
     */
    protected int[] readCoords(){
        int[] result = new int[4];
        result[0] = (Integer) xStartSpinner.getValue();
        result[1] = (Integer) yStartSpinner.getValue();
        result[2] = (Integer) xEndSpinner.getValue();
        result[3] = (Integer) yEndSpinner.getValue();
        return result;
    }

    public static void main(String[] args){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        MainForm form = new MainForm();
    }
}
