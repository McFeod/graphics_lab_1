import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import java.awt.*;

/**
 * Отрисовка отрезка, координат и т.п.
 */
public class Renderer {
    static int width, height, cells=33;
    static float step;      // расстояние между линиями сетки

    /**
     * получение абсциссы левого угла клетки
     * @param coord - логическая координата
     * @return - физическая координата
     */
    protected static float getX(int coord){
        return width/2 + coord*step - step/2;
    }

    /**
     * получение ординаты верхнего угла клетки
     * @param coord - логическая координата
     * @return - физическая координата
     */
    protected static float getY(int coord){
        return height/2 + coord*step - step/2;
    }

    /**
     * Закрашивание клетки
     * @param gl2 - холст
     * @param xCoord - абсцисса (логическая)
     * @param yCoord - ордината (логическая)
     */
    protected static void pseudoPoint(GL2 gl2, int xCoord, int yCoord){
        float x = getX(xCoord);
        float y = getY(yCoord);
        gl2.glBegin(GL2.GL_QUADS);
        gl2.glColor3f(0f, 0.9f, 0f);
        gl2.glVertex2f(x, y);
        gl2.glVertex2f(x, y+step);
        gl2.glVertex2f(x+step, y+step);
        gl2.glVertex2f(x+step, y);
        gl2.glEnd();
    }

    /**
     * Инициализация холста
     * @param gl2 - сам холст
     * @param width - его ширина
     * @param height - его длина
     */
    protected static void setup(GL2 gl2, int width, int height, int cells){
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        Renderer.width = width;
        Renderer.height = height;
        Renderer.cells = cells;
        step = Float.max(Float.min(((float)height)/cells, (float)width/cells), 2f);
        GLU glu = new GLU();
        glu.gluOrtho2D( 0.0f, width, 0.0f, height );
        gl2.glMatrixMode( GL2.GL_MODELVIEW );
        gl2.glLoadIdentity();
        gl2.glViewport( 0, 0, width, height );
    }

    /**
     * Перерисовка
     * @param gl2 - холст
     * @param coords - массив с координатами начала и конца отрезка
     */
    protected static void render(GL2 gl2, int[] coords, int cells) {
        Renderer.cells = cells;
        step = Float.max(Float.min(((float)height)/cells, (float)width/cells), 2f);
        makeBackground(gl2);
        Bresenham bresenham = new Bresenham(coords);
        for (Point p: bresenham) {
            pseudoPoint(gl2, p.x, p.y);
        }
    }

    /**
     * Отрисовка осей, сетки и т.п.
     * @param gl2 - холст
     */
    protected static void makeBackground(GL2 gl2){
        gl2.glClear( GL.GL_COLOR_BUFFER_BIT );
        gl2.glLoadIdentity();

        // фон
        gl2.glBegin(GL2.GL_QUADS);
        gl2.glColor3f(0.2f, 0.2f, 0.2f);
        gl2.glVertex2f(getX(-cells), getY(-cells));
        gl2.glVertex2f(getX(-cells), getY(cells));
        gl2.glVertex2f(getX(cells), getY(cells));
        gl2.glVertex2f(getX(cells), getY(-cells));
        gl2.glEnd();

        // оси
        gl2.glBegin(GL.GL_LINES);
        gl2.glColor3f(1f, 0f, 0f);
        gl2.glVertex2f(width/2, 0);
        gl2.glVertex2f(width/2, height);
        gl2.glVertex2f(0, height/2);
        gl2.glVertex2f(width, height/2);
        gl2.glEnd();

        // сетка
        gl2.glBegin( GL.GL_LINES );
        float k = Float.max(100f-cells, 10)/100f;
        gl2.glColor3f(k*0.5f, k*1f, k*0.5f);
        for(int i=0; i<cells; i+=1) {
            gl2.glVertex2f(getX(i), getY(-cells));
            gl2.glVertex2f(getX(i), getY(cells));
            gl2.glVertex2f(getX(-i), getY(-cells));
            gl2.glVertex2f(getX(-i), getY(cells));
        }
        for(int i=0; i<cells; i+=1) {
            gl2.glVertex2f(getX(-cells), getY(i));
            gl2.glVertex2f(getX(cells), getY(i));
            gl2.glVertex2f(getX(-cells), getY(-i));
            gl2.glVertex2f(getX(cells), getY(-i));
        }
        gl2.glEnd();
    }
}