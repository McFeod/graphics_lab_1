import java.awt.*;
import java.util.Iterator;

import static java.lang.Math.abs;

/**
 * Ленивая коллекция из точек прямой
 */
public class Bresenham implements Iterable<Point>{
    private Point start, end; // храним концы отрезка
    public Bresenham(int[] coords){
        start = new Point(coords[0], coords[1]);
        end = new Point(coords[2], coords[3]);
    }


    class BresenhamIterator implements Iterator<Point> {
        private Bresenham points;
        int longSide, shortSide, directionX, directionY, mainX, mainY, cursor = 0, x, y, temp;

        private int sign(int num) {
            return (num > 0) ? 1 : (num < 0) ? -1 : 0;
        }

        /**
         * Инициализация итератора
         */
        public BresenhamIterator(Bresenham points) {
            this.points = points;
            int deltaX = points.end.x - points.start.x;
            int deltaY = points.end.y - points.start.y;
            directionX = sign(deltaX);  // направление, в котором идут диагональные участки
            directionY = sign(deltaY);
            deltaX = abs(deltaX);
            deltaY = abs(deltaY);
            if (deltaX > deltaY) {
                longSide = deltaX;      // тангенс больше единицы
                shortSide = deltaY;
                mainX = directionX;     // есть горизонтальные участки
                mainY = 0;              // нет вертикальных
            } else {
                longSide = deltaY;      // наоборот
                shortSide = deltaX;
                mainY = directionY;
                mainX = 0;
            }
            x = points.start.x;
            y = points.start.y;
            temp = longSide / 2;
        }

        @Override
        public boolean hasNext() {
            return cursor <= longSide;
        }

        @Override
        public Point next() {
            Point current = new Point(x, y);
            cursor++;
            temp -= shortSide;
            if (temp < 0) {          // нужен сдвиг по обеим осям
                temp += longSide;
                x += directionX;
                y += directionY;
            } else {                // только по одной оси
                x += mainX;
                y += mainY;
            }
            return current;
        }
    }

    @Override
    public Iterator<Point> iterator() {
        return new BresenhamIterator(this);
    }

}
